import json
import logging
import os
import threading
import re

import time

import sys
from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

from g_drive import GDriveDownloader
from dropbox_downloader import DropBoxDownloader


ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)

inst_g = GDriveDownloader()
inst_d = DropBoxDownloader()

cur_dir = os.path.dirname(os.path.realpath(__file__))
if cur_dir[-1] != '/':
    cur_dir += '/'


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/')
def form():
    return render_template('index.html')


@app.route('/g_connect')
def g_connect():
    if inst_g.connect():
        print 'Pulled data from Google Drive successfully...'
        return json.dumps('true')
    else:
        print "Failed to pull data from Google Drive."
        return json.dumps('false')


@app.route('/d_connect')
def d_connect():
    if inst_d.connect():
        print 'Pulled data from Dropbox successfully...'
        return json.dumps('true')
    else:
        print "Failed to pull data from Dropbox."
        return json.dumps('false')


@app.route('/gdrive')
def goto_gdrive():
    return render_template('gdrive.html')


@app.route('/dropbox')
def goto_dropbox():
    return render_template('dropbox.html')


@app.route('/update_g_item')
def update_g_item():
    _id = request.args.get('id')
    val = request.args.get('val')

    print "Updating item, id: ", _id, ', val: ', val

    cur_item = inst_g.get_item_by_id(_id)

    path = cur_dir + 'g_drive_item.txt'
    f = open(path, 'r+b')
    f_content = f.readlines()
    if val == 'true':
        if _id not in ''.join(f_content):
            f_content.append('\n' + _id + ',' + cur_item['name'] + ',' + cur_item['mimeType'])
    else:
        if _id in ''.join(f_content):
            for line in f_content:
                if _id in line:
                    f_content.remove(line)
                    break

    f.seek(0)
    f.truncate()
    f.write(''.join(f_content))
    f.close()
    return json.dumps('true')


@app.route('/update_d_item')
def update_d_item():
    _id = request.args.get('id')
    val = request.args.get('val')

    print "Updating item, id: ", _id, ', val: ', val

    path = cur_dir + 'dropbox_item.txt'
    f = open(path, 'r+b')
    f_content = f.readlines()
    if val == 'true':
        if _id not in ''.join(f_content):
            f_content.append('\n' + _id)
    else:
        if _id in ''.join(f_content):
            for line in f_content:
                if _id in line:
                    f_content.remove(line)
                    break

    f.seek(0)
    f.truncate()
    f.write(''.join(f_content))
    f.close()
    return json.dumps('true')


@app.route('/get_interval', methods=['GET'])
def get_interval():
    interval = inst_g.get_param_from_xml('INTERVAL')
    if interval == '60':
        interval = 'hour'
    elif interval == '1440':
        interval = 'day'
    else:
        interval = 'weekly'
    return json.dumps(interval)


@app.route('/update_g_user', methods=['POST'])
def update_g_user_credential():
    file = request.files['json_file']
    email = request.form['email']
    pwd = request.form['password']

    if not re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email):
        return render_template('setup_gdrive.html', status='Invalid type of email')

    print 'New credential: ', email, ',  ', pwd

    # if '' == '':
    try:
        # Make the filename safe, remove unsupported chars
        filename = secure_filename(file.filename)

        if filename[-5:] != '.json':
            return render_template('setup_gdrive.html', status='Please select json file')

        content = file.read()
        print '-- File Content --'
        print content, '\n'

        f = open(cur_dir + 'gdrive_credential.json', 'r+b')
        f.seek(0)
        f.truncate()
        f.write(content)
        f.close()

        inst_g.set_param_to_xml('GDRIVE_EMAIL', email)
        inst_g.set_param_to_xml('GDRIVE_PWD', pwd)

        print 'Succeeded to update user credentails'

        return render_template('setup_gdrive.html', status='Succeeded to update user credential.')

    except:
        print "Failed to update..."
        return render_template('setup_gdrive.html', status='Failed to update.')


@app.route('/update_dropbox_token', methods=['POST'])
def update_dropbox_token():
    token = request.form['txt_token']

    print 'New Dropbox token: ', token

    # if '' == '':
    try:
        inst_g.set_param_to_xml('DROPBOX_TOKEN', token)
        print 'Succeeded to update dropbox token'

        return render_template('setup_dropbox.html', status='Succeeded to update Token.')

    except:
        print "Failed to update..."
        return render_template('setup_dropbox.html', status='Failed to update.')


@app.route('/update_interval')
def update_interval():
    new_interval = request.args.get('val')
    if new_interval == 'hour':
        new_interval = 60   # value in minute
    elif new_interval == 'day':
        new_interval = 60 * 24
    elif new_interval == 'weekly':
        new_interval = 60 * 24 * 7

    inst_g.set_param_to_xml('INTERVAL', new_val=str(new_interval))
    return json.dumps('true')


@app.route('/setup_gdrive')
def setup_gdrive():
    return render_template('setup_gdrive.html')


@app.route('/setup_onedrive')
def setup_onedrive():
    return render_template('setup_onedrive.html')


@app.route('/setup_dropbox')
def setup_dropbox():
    return render_template('setup_dropbox.html')


@app.route('/get_g_dir', methods=['GET'])
def get_gdrive():
    """
    Sample response is:
    {
        u'files': [
                    {
                        u'mimeType': u'image/jpeg',
                        u'kind': u'drive#file',
                        u'id': u'0ByrPcbC-4pd5WlFETFFCUTYtRFU',
                        u'name': u'1.jpg'},
                    {
                        u'mimeType': u'application/vnd.google-apps.folder',
                        u'kind': u'drive#file',
                        u'id': u'0ByrPcbC-4pd5VjhYUmh2NzVMOFk',
                        u'name': u'New Folder'},
                    {
                        u'mimeType': u'application/vnd.google-apps.spreadsheet',
                        u'kind': u'drive#file',
                        u'id': u'1sJKIGhjvdoq3sWn8p95Ynm6xNg62VGWhsOxnVmHBQI8',
                        u'name': u'TEST Sheet'},
                    {
                        u'mimeType': u'application/vnd.google-apps.document',
                        u'kind': u'drive#file',
                        u'id': u'1v77AKFgqvJKlKpvB6edW4fEHik4-hCG7G8VJK6XA_Ew',
                        u'name': u'TEST Document'},
                    {
                        u'mimeType': u'application/pdf',
                        u'kind': u'drive#file', u'id': u'0ByrPcbC-4pd5c3RhcnRlcl9maWxl',
                        u'name': u'Getting started'}
                  ],
        u'kind': u'drive#fileList'
    }
    :return:
    """
    if request.method == 'GET':
        folder_id = request.args.get('id')

        f = open(cur_dir + 'g_drive_item.txt', 'r+b')
        f_content = f.readlines()

        full_list = inst_g.get_child_list(folder_id)
        response = []
        for item in full_list:
            tmp = dict()
            tmp['id'] = item['id']
            tmp['name'] = item['name']
            if item['mimeType'] == 'application/vnd.google-apps.folder':
                # continue
                tmp['type'] = 'folder'
            else:
                tmp['type'] = item['mimeType']
            if item['id'] in ''.join(f_content):
                tmp['active'] = 'true'
            else:
                tmp['active'] = 'false'

            response.append(tmp)

        return json.dumps(response)


@app.route('/get_d_dir', methods=['GET'])
def get_dropbox():
    """
    Sample response is:

    :return:
    """
    if request.method == 'GET':
        folder_path = request.args.get('path')
        folder_path = folder_path.replace('_', '/')
        f = open(cur_dir + 'dropbox_item.txt', 'r+b')
        f_content = f.readlines()

        child_list = inst_d.get_child(folder_path)

        response = []
        for item in child_list:
            tmp = dict()
            path = item['path']
            tmp['path'] = path
            tmp['name'] = path.split('/')[-1]
            if item['is_dir']:
                # continue
                tmp['type'] = 'folder'
            else:
                tmp['type'] = 'file'
            if item['path'] in ''.join(f_content):
                tmp['active'] = 'true'
            else:
                tmp['active'] = 'false'

            response.append(tmp)

        return json.dumps(response)


@app.route('/get_g_parent_ids', methods=['GET'])
def get_parent_id_list():
    """
    Get ID list of parent items.
    :return:
    """
    _id = request.args.get('id')
    p_list = inst_g.get_parent_id_list(_id)

    return json.dumps(p_list)


def download_files(*args):
    """
    Download files and save to external USB driver.
    :param args:
    :return:
    """
    if not inst_g.debug:
        while True:
            interval = inst_g.get_param_from_xml('INTERVAL')
            try:
                interval = int(interval)
            except ValueError:
                interval = 10
            s_time = time.time()

            base_path = '/media/' + os.listdir('/media')[0] + '/'

            # -- Download Google Drive files --
            if not inst_g.connect():
                print "Failed to connect to Google Drive"
            else:
            # try:
                drive_path = base_path + os.listdir(base_path)[0] + 'google_drive/'
                print "Destination path: ", drive_path
                path = cur_dir + 'g_drive_item.txt'
                f = open(path, 'r')
                f_content = f.readlines()
                f.close()
                for line in f_content:
                    if len(line.strip()) > 0:
                        [_id, _name, _mime] = line.split(',')
                        inst_g.download_item(_id, _mime, drive_path + inst_g.get_full_path(_id))

            # except:
            #     print 'Error, failed to download...'

            # -- Download DropBox --

            if not inst_d.connect():
                print "Failed to connect to Dropbox..."
            else:
                drive_path = base_path + os.listdir(base_path)[0] + 'dropbox'
                print "Destination path: ", drive_path
                path = cur_dir + 'dropbox_item.txt'
                f = open(path, 'r')
                f_content = f.readlines()
                f.close()
                for line in f_content:
                    if len(line.strip()) > 0:
                        metadata = inst_d.get_metadata(line)
                        if metadata['is_dir']:
                            inst_d.download_directory(metadata['path'], drive_path)
                        else:
                            inst_d.download_file(metadata['path'], drive_path)

            remaining = interval * 60 + s_time - time.time()
            if remaining > 0:
                count_down(int(remaining))


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print


if __name__ == '__main__':

    log_file_name = 'log.txt'
    logging.basicConfig(level=10, filename=log_file_name, format='%(asctime)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info(" ---- Server is started ---- ")

    print "Server is started now..."

    threading.Thread(target=download_files).start()

    if os.name == 'nt':
        port = 8080
    else:
        port = 80
    app.run(
        host="0.0.0.0",
        port=port
    )
