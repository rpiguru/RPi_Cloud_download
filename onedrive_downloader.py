import onedrivesdk

redirect_uri = 'http://localhost:8080/'
client_secret = 'Zw2PGfYUauY17skF5rsWrsO'
client_id = '297ba9de-41c0-419d-9f81-2e9df48377fb'
api_base_url = 'https://api.onedrive.com/v1.0/'
scopes = ['wl.signin', 'wl.offline_access', 'onedrive.readwrite']

http_provider = onedrivesdk.HttpProvider()
auth_provider = onedrivesdk.AuthProvider(
    http_provider=http_provider,
    client_id=client_id,
    scopes=scopes)

client = onedrivesdk.OneDriveClient(api_base_url, auth_provider, http_provider)
auth_url = client.auth_provider.get_auth_url(redirect_uri)
# Ask for the code
print('Paste this URL into your browser, approve the app\'s access.')
print('Copy everything in the address bar after "code=", and paste it below.')
print(auth_url)
code = input('Paste code here: ')

client.auth_provider.authenticate(code, redirect_uri, client_secret)

root_folder = client.item(drive='me', id='root').children.get()

print root_folder

# id_of_file = root_folder[0].id

# client.item(drive='me', id=id_of_file).download('./path_to_download_to.txt')
